<?php
include '../db/koneksi.php';
include '../controller/Province.php';
include '../controller/People.php';
include '../controller/Region.php';

if (isset($_GET['form'])) {
    $form = $_GET['form'];
} elseif (isset($_POST['form'])) {
    $form = $_POST['form'];
}

	// var_dump($_POST);die();
$province = new Province($db);
if ($form == 'input_provinsi') {
	$province->store($_POST);
} elseif ($form == 'edit_provinsi') {
	$province->update($_POST);
}

	// var_dump($_POST);die();
$region = new Region($db);
if ($form == 'input_kabupaten') {
	$region->store($_POST);
} elseif ($form == 'edit_kabupaten') {
	$region->update($_POST);
}

	// var_dump($_POST);die();
$people = new People($db);
if ($form == 'input_villager') {
	$people->store($_POST);
} elseif ($form == 'edit_villager') {
	$people->update($_POST);
}