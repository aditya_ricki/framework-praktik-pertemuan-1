<?php
include '../db/koneksi.php';
include '../controller/Province.php';
include '../controller/People.php';
include '../controller/Region.php';

if (isset($_GET['form'])) {
    $form = $_GET['form'];
} elseif (isset($_POST['form'])) {
    $form = $_POST['form'];
}

$province = new Province($db);
if ($form == 'get_provinsi') {
	$province->edit($_GET);
} elseif ($form == 'delete_province') {
	$province->delete($_POST);
}

$region = new Region($db);
if ($form == 'get_region') {
	$region->edit($_GET);
} elseif ($form == 'delete_region') {
	$region->delete($_POST);
}

$people = new People($db);
if ($form == 'get_villager') {
	$people->edit($_GET);
} elseif ($form == 'delete_villager') {
	$people->delete($_POST);
}