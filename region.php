<?php
	include 'db/koneksi.php';
	include 'controller/Province.php';
	include 'controller/Region.php';
	include 'includes/head.php';
	include 'includes/navbar.php';

	$provinces     = new Province($db);
	$regions       = new Region($db);
	$data          = $regions->index();
	$data_provinsi = $provinces->index();
?>
	<!-- Modal -->
	<div class="modal fade" id="tambahKabupaten" tabindex="-1" aria-labelledby="tambahKabupatenLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
		        <form action="controller/Store.php" method="post">
		      		<div class="modal-header">
	        			<h5 class="modal-title" id="tambahKabupatenLabel">Tambah Kabupaten</h5>
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          	<span aria-hidden="true">&times;</span>
		        		</button>
		      		</div>
		      		<div class="modal-body">
					  	<div class="form-group">
					      	<input type="text" name="region_name" class="form-control" id="inputNamaKabupaten" placeholder="Nama Kabupaten">
					  	</div>
						<div class="form-group">
						    <select class="form-control" id="inputIdProvinsi" name="province_id">
						    	<option value="">--PILIH--</option>
						    	<?php foreach ($data_provinsi as $value) : ?>
						    	<option value="<?= $value['province_id']; ?>"><?= $value['province_name'] ?></option>
						    	<?php endforeach; ?>
						    </select>
						</div>
		      		</div>
			      	<div class="modal-footer">
			      		<input type="hidden" name="form" value="input_kabupaten">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Simpan</button>
			      	</div>
				</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editKabupaten" tabindex="-1" aria-labelledby="editKabupatenLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
		        <form action="controller/Store.php" method="post">
		      		<div class="modal-header">
	        			<h5 class="modal-title" id="editKabupatenLabel">Edit Kabupaten</h5>
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          	<span aria-hidden="true">&times;</span>
		        		</button>
		      		</div>
		      		<div class="modal-body">
					  	<div class="form-group">
					      	<input type="text" name="region_name" class="form-control" id="editNamaKabupaten" placeholder="Nama Kabupaten">
					  	</div>
						<div class="form-group">
						    <select class="form-control" id="editIdProvinsi" name="province_id">
						    	<?php foreach ($data_provinsi as $value) : ?>
						    	<option value="<?= $value['province_id']; ?>"><?= $value['province_name'] ?></option>
						    	<?php endforeach; ?>
						    </select>
						</div>
		      		</div>
			      	<div class="modal-footer">
			      		<input type="hidden" name="id" id="id_kabupaten">
			      		<input type="hidden" name="form" value="edit_kabupaten">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Simpan</button>
			      	</div>
				</form>
	    	</div>
	  	</div>
	</div>

	<div class="container mt-5">
		<div class="row mb-3">
			<div class="col">
				<div class="float-left">
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahKabupaten">
					 	Tambah Data
					</button>
				</div>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead>
					<th>No</th>
					<th>Nama Kabupaten</th>
					<th>Nama Provinsi</th>
					<th>Aksi</th>
				</thead>
				<tbody>
					<?php if ($data == null) : ?>
						<tr>
							<td colspan="4" class="text-center">Data tidak ada!</td>
						</tr>
					<?php endif; $no = 1; ?>
					<?php foreach ($data as $value) : ?>
					<tr id="tr-<?= $value['region_id'] ?>">
						<td><?= $no ?></td>
						<td><?= $value['region_name'] ?></td>
						<td><?= $value['province_name'] ?></td>
						<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editKabupaten" data-id="<?= $value['region_id']; ?>"><i class="fa fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-id="<?= $value['region_id']; ?>"><i class="fa fa-trash"></i></button>
                            </div>
						</td>
					</tr>
					<?php $no++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
	<!-- <script src="includes/js/jquery.js"></script>
	<script src="includes/js/jquery.min.js"></script> -->

	<script>
		$(document).ready(() => {
	        $(document).on("click", ".btn-warning", function() {
	            let id_region = $(this).data('id')
	            $.ajax({ //create an ajax request to display.php
	                type: "GET",
	                url: "controller/Ajax.php",
	                data: {
	                    form: 'get_region',
	                    id: id_region
	                },
	                dataType: "json",
	                success: function(response) {
	                    $("#editNamaKabupaten").val(response.region_name);
	                    $("#editIdProvinsi").val(response.province_id);
	                },
	                error: (error) => {
	                    console.log(error)
	                }
	            })
	        })

	        $(document).on("click", ".btn-danger", function() {
	            let id_region = $(this).data('id')
	            $(`#tr-${id_region}`).remove();
	            $.ajax({ //create an ajax request to display.php
	                type: "POST",
	                url: "controller/Ajax.php",
	                data: {
	                    form: 'delete_region',
	                    id: id_region
	                },
	                dataType: "json",
	                success: () => {
	                    console.log('fug')
	                }
	            })
	        })
		})
	</script>

<?php
	include 'includes/footer.php';
?>