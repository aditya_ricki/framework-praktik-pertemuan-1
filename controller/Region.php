<?php

/**
 *
 */
class Region
{
	protected $db;

	function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
		$query  = "SELECT * FROM regions INNER JOIN provinces ON regions.province_id = provinces.province_id";
		$result = $this->db->query($query);

		$data   = array();

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        return $data;
	}

	public function store($params)
	{
		$region_name = $params['region_name'];
        $province_id = $params['province_id'];

        try {
            $query = "INSERT INTO regions (region_name, province_id) VALUES ('$region_name', '$province_id')";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        header("Location: ../region.php");
        die();
	}

	public function edit($params)
	{
		$id = $params['id'];

        $query = "SELECT * FROM regions INNER JOIN provinces ON regions.province_id = provinces.province_id WHERE region_id = $id";
        $result = $this->db->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo json_encode($row);
            die();
        }
	}

	public function update($params)
	{
		$region_name = $params['region_name'];
		$id            = $params['id'];

        try {
            $query = "UPDATE regions SET region_name='$region_name' WHERE region_id='$id'";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        header("Location: ../region.php");
        die();
	}

    public function delete($params)
    {
		$id = $params['id'];
        try {
            $query = "DELETE FROM regions WHERE region_id='$id'";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die();
        }
        header("Location: ../region.php");
        die();
    }
}