<?php

/**
 *
 */
class Province
{
	protected $db;

	function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
		$query  = "SELECT * FROM provinces";
		$result = $this->db->query($query);

		$data   = array();

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        return $data;
	}

	public function store($params)
	{
		$province_name = $params['province_name'];

        try {
            $query = "INSERT INTO provinces (province_name) VALUES ('$province_name')";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        header("Location: ../province.php");
        die();
	}

	public function edit($params)
	{
		$id = $params['id'];

        $query = "SELECT * FROM provinces WHERE province_id = $id";
        $result = $this->db->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo json_encode($row);
            die();
        }
	}

	public function update($params)
	{
		$province_name = $params['province_name'];
		$id            = $params['id'];

        try {
            $query = "UPDATE provinces SET province_name='$province_name' WHERE province_id='$id'";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        header("Location: ../province.php");
        die();
	}

    public function delete($params)
    {
		$id = $params['id'];
        try {
            $query = "DELETE FROM provinces WHERE province_id='$id'";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die();
        }
        header("Location: ../province.php");
        die();
    }
}