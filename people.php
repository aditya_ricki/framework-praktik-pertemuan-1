<?php
	include 'db/koneksi.php';
	include 'controller/Province.php';
	include 'controller/Region.php';
	include 'controller/People.php';
	include 'includes/head.php';
	include 'includes/navbar.php';

	$provinces     = new Province($db);
	$regions       = new Region($db);
	$peoples       = new People($db);
	$data_region   = $regions->index();
	$data_provinsi = $provinces->index();
	$data          = $peoples->index();
?>
	<!-- Modal -->
	<div class="modal fade" id="tambahVillager" tabindex="-1" aria-labelledby="tambahVillagerLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
		        <form action="controller/Store.php" method="post">
		      		<div class="modal-header">
	        			<h5 class="modal-title" id="tambahVillagerLabel">Tambah Villager</h5>
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          	<span aria-hidden="true">&times;</span>
		        		</button>
		      		</div>
		      		<div class="modal-body">
					  	<div class="form-group">
					      	<input type="text" name="villager_name" class="form-control" id="inputNamaVillager" placeholder="Nama Villager">
					  	</div>
						<div class="form-group">
						    <select class="form-control" id="inputVillagerGender" name="villager_gender">
						    	<option value="">--JENIS KELAMIN--</option>
						    	<option value="M">Laki-laki</option>
						    	<option value="F">Perempuan</option>
						    </select>
						</div>
						<div class="form-group">
						    <select class="form-control" id="inputIdRegion" name="region_id">
						    	<option value="">--KABUPATEN--</option>
						    	<?php foreach ($data_region as $value) : ?>
						    	<option value="<?= $value['region_id']; ?>"><?= $value['region_name'] ?></option>
						    	<?php endforeach; ?>
						    </select>
						</div>
		      		</div>
			      	<div class="modal-footer">
			      		<input type="hidden" name="form" value="input_villager">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Simpan</button>
			      	</div>
				</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editVillager" tabindex="-1" aria-labelledby="editVillagerLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
		        <form action="controller/Store.php" method="post">
		      		<div class="modal-header">
	        			<h5 class="modal-title" id="editVillagerLabel">Edit Villager</h5>
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          	<span aria-hidden="true">&times;</span>
		        		</button>
		      		</div>
		      		<div class="modal-body">
					  	<div class="form-group">
					      	<input type="text" name="villager_name" class="form-control" id="editNamaVillager" placeholder="Nama Villager">
					  	</div>
						<div class="form-group">
						    <select class="form-control" id="editVillagerGender" name="villager_gender">
						    	<option value="">--JENIS KELAMIN--</option>
						    	<option value="M">Laki-laki</option>
						    	<option value="F">Perempuan</option>
						    </select>
						</div>
						<div class="form-group">
						    <select class="form-control" id="editIdRegion" name="region_id">
						    	<option value="">--KABUPATEN--</option>
						    	<?php foreach ($data_region as $value) : ?>
						    	<option value="<?= $value['region_id']; ?>"><?= $value['region_name'] ?></option>
						    	<?php endforeach; ?>
						    </select>
						</div>
		      		</div>
			      	<div class="modal-footer">
			      		<input type="hidden" name="id" id="id_villager">
			      		<input type="hidden" name="form" value="edit_villager">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Simpan</button>
			      	</div>
				</form>
	    	</div>
	  	</div>
	</div>

	<div class="container mt-5">
		<div class="row mb-3">
			<div class="col">
				<div class="float-left">
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahVillager">
					 	Tambah Data
					</button>
				</div>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead>
					<th>No</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
					<th>Kabupaten</th>
					<th>Provinsi</th>
					<th>Aksi</th>
				</thead>
				<tbody>
					<?php if ($data == null) : ?>
						<tr>
							<td colspan="6" class="text-center">Data tidak ada!</td>
						</tr>
					<?php endif; $no = 1; ?>
					<?php foreach ($data as $value) : ?>
					<tr id="tr-<?= $value['villager_id'] ?>">
						<td><?= $no ?></td>
						<td><?= $value['villager_name'] ?></td>
						<?php if ($value['villager_gender'] == 'M') : ?>
							<td>LAKI-LAKI</td>
						<?php else : ?>
							<td>PEREMPUAN</td>
						<?php endif; ?>
						<td><?= $value['region_name'] ?></td>
						<td><?= $value['province_name'] ?></td>
						<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editVillager" data-id="<?= $value['villager_id']; ?>"><i class="fa fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-id="<?= $value['villager_id']; ?>"><i class="fa fa-trash"></i></button>
                            </div>
						</td>
					</tr>
					<?php $no++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
	<!-- <script src="includes/js/jquery.js"></script>
	<script src="includes/js/jquery.min.js"></script> -->

	<script>
		$(document).ready(() => {
	        $(document).on("click", ".btn-warning", function() {
	            let id_villager = $(this).data('id')
	            $.ajax({ //create an ajax request to display.php
	                type: "GET",
	                url: "controller/Ajax.php",
	                data: {
	                    form: 'get_villager',
	                    id: id_villager
	                },
	                dataType: "json",
	                success: function(response) {
	                    $("#editNamaVillager").val(response.villager_name);
	                    $('#editVillagerGender').val(response.villager_gender);
	                    $("#editIdRegion").val(response.region_id);
	                    $("#id_villager").val(response.villager_id);
	                },
	                error: (error) => {
	                    console.log(error)
	                }
	            })
	        })

	        $(document).on("click", ".btn-danger", function() {
	            let id_villager = $(this).data('id')
	            $(`#tr-${id_villager}`).remove();
	            $.ajax({ //create an ajax request to display.php
	                type: "POST",
	                url: "controller/Ajax.php",
	                data: {
	                    form: 'delete_villager',
	                    id: id_villager
	                },
	                dataType: "json",
	                success: () => {
	                    console.log('fug')
	                }
	            })
	        })
		})
	</script>

<?php
	include 'includes/footer.php';
?>