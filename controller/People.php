<?php

/**
 *
 */
class People
{
	protected $db;

	function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
		$query  = "SELECT * FROM villagers INNER JOIN regions ON villagers.region_id = regions.region_id INNER JOIN provinces ON regions.province_id = provinces.province_id";
		$result = $this->db->query($query);

		$data   = array();

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        return $data;
	}

	public function store($params)
	{
		$villager_name = $params['villager_name'];
        $villager_gender = $params['villager_gender'];
        $region_id = $params['region_id'];

        try {
            $query = "INSERT INTO villagers (villager_name, region_id, villager_gender) VALUES ('$villager_name', '$region_id', '$villager_gender')";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        header("Location: ../people.php");
        die();
	}

	public function edit($params)
	{
		$id = $params['id'];

        $query = "SELECT * FROM villagers INNER JOIN regions ON villagers.region_id = regions.region_id INNER JOIN provinces ON regions.province_id = provinces.province_id WHERE villager_id = $id";
        $result = $this->db->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo json_encode($row);
            die();
        }
	}

	public function update($params)
	{
        // var_dump($params);die();
        $villager_name   = $params['villager_name'];
        $villager_gender = $params['villager_gender'];
        $villager_id     = $params['id'];
        $region_id       = $params['region_id'];

        try {
            $query = "UPDATE villagers SET villager_name='$villager_name', villager_gender='$villager_gender', region_id='$region_id' WHERE villager_id='$villager_id'";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        header("Location: ../people.php");
        die();
	}

    public function delete($params)
    {
		$id = $params['id'];
        try {
            $query = "DELETE FROM villagers WHERE villager_id='$id'";
            $result = $this->db->query($query);
        } catch (\Throwable $th) {
            print_r($th);
            die();
        }
        header("Location: ../people.php");
        die();
    }
}